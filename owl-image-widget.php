<?php
/*
Plugin Name: OWL-Image-Slider
Plugin URI: http://www.google.de
Description: Ein Plugin das Bilder als Links in einer Slideshow anzeigen kann. Voll responsiv.
Version: 1.1
Author: Patrick
Author URI: http://www.google.de
License: GPL2
*/
?>
<?php
class owl_image_slider extends WP_Widget {

	// constructor
	function owl_image_slider() {
		parent::WP_Widget(false, $name = __('OWL Image Slider', 'wp_widget_plugin') );
	}
	
	// widget form creation
	function form($instance) {	
		$widget_defaults = array(
			'title'         =>   'Upcoming Events',
			'number_events' =>   5
		);
	$instance  = wp_parse_args( (array) $instance, $widget_defaults );
	pco_image_field( $this, $instance, array( 'field' => 'image1' ) );
	
	?>
	<p>
    <label for="<?php echo $this->get_field_id( 'Image1Link' ); ?>"><?php _e( 'Image1Link', 'uep' ); ?></label>
    <input type="text" id="<?php echo $this->get_field_id( 'Image1Link' ); ?>" name="<?php echo $this->get_field_name( 'Image1Link' ); ?>" class="widefat" value="<?php echo esc_attr( $instance['Image1Link'] ); ?>">
	</p>
	<?php
	
	pco_image_field( $this, $instance, array( 'field' => 'image2' ) );
	?>
	<p>
    <label for="<?php echo $this->get_field_id( 'Image2Link' ); ?>"><?php _e( 'Image2Link', 'uep' ); ?></label>
    <input type="text" id="<?php echo $this->get_field_id( 'Image2Link' ); ?>" name="<?php echo $this->get_field_name( 'Image2Link' ); ?>" class="widefat" value="<?php echo esc_attr( $instance['Image2Link'] ); ?>">
	</p>
	<?php
	pco_image_field( $this, $instance, array( 'field' => 'image3' ) );
	?>
	<p>
    <label for="<?php echo $this->get_field_id( 'Image3Link' ); ?>"><?php _e( 'Image3Link', 'uep' ); ?></label>
    <input type="text" id="<?php echo $this->get_field_id( 'Image3Link' ); ?>" name="<?php echo $this->get_field_name( 'Image3Link' ); ?>" class="widefat" value="<?php echo esc_attr( $instance['Image3Link'] ); ?>">
	</p>
	<?php
	pco_image_field( $this, $instance, array( 'field' => 'image4' ) );
	?>
	<p>
    <label for="<?php echo $this->get_field_id( 'Image4Link' ); ?>"><?php _e( 'Image4Link', 'uep' ); ?></label>
    <input type="text" id="<?php echo $this->get_field_id( 'Image4Link' ); ?>" name="<?php echo $this->get_field_name( 'Image4Link' ); ?>" class="widefat" value="<?php echo esc_attr( $instance['Image4Link'] ); ?>">
	</p>
	<?php
	pco_image_field( $this, $instance, array( 'field' => 'image5' ) );
	?>
	<p>
    <label for="<?php echo $this->get_field_id( 'Image5Link' ); ?>"><?php _e( 'Image5Link', 'uep' ); ?></label>
    <input type="text" id="<?php echo $this->get_field_id( 'Image5Link' ); ?>" name="<?php echo $this->get_field_name( 'Image5Link' ); ?>" class="widefat" value="<?php echo esc_attr( $instance['Image5Link'] ); ?>">
	</p>
	<?php
	pco_image_field( $this, $instance, array( 'field' => 'image6' ) );
	?>
	<p>
    <label for="<?php echo $this->get_field_id( 'Image6Link' ); ?>"><?php _e( 'Image6Link', 'uep' ); ?></label>
    <input type="text" id="<?php echo $this->get_field_id( 'Image6Link' ); ?>" name="<?php echo $this->get_field_name( 'Image6Link' ); ?>" class="widefat" value="<?php echo esc_attr( $instance['Image6Link'] ); ?>">
	</p>
	<p>
	<input id="<?php echo $this->get_field_id('randomize'); ?>" name="<?php echo $this->get_field_name('randomize'); ?>" type="checkbox" value="1" <?php checked( '1', $instance["randomize"] ); ?> />
	<label for="<?php echo $this->get_field_id('randomize'); ?>"><?php _e('randomize', 'wp_widget_plugin'); ?></label>
	</p>
	
	
	<?php

	}

	// widget update
	function update($new_instance, $old_instance) {
    $instance = $old_instance;
 
    $instance['image1'] = $new_instance['image1'];    
	$instance['image2'] = $new_instance['image2'];    
	$instance['image3'] = $new_instance['image3'];    
	$instance['image4'] = $new_instance['image4'];	
	$instance['image5'] = $new_instance['image5'];    
	$instance['image6'] = $new_instance['image6'];
	$instance['Image1Link']  = $new_instance['Image1Link']; 
	$instance['Image2Link']  = $new_instance['Image2Link']; 
	$instance['Image3Link']  = $new_instance['Image3Link']; 
	$instance['Image4Link']  = $new_instance['Image4Link']; 
	$instance['Image5Link']  = $new_instance['Image5Link']; 
	$instance['Image6Link']  = $new_instance['Image6Link']; 
	$instance['randomize'] = $new_instance['randomize']; 

 
    return $instance;
	}

	// widget display
	function widget($args, $instance) {
	?>
	<aside id="<?php echo $this->id ?>" class="widget ">
		<div id="<?php echo $this->id ?>-div" class="owl-carousel" style="text-align:center;">
				<?php				
					$images = [];
					$links = [];
					$randomize = $instance['randomize'];
					if ($instance['image1'])
						$images[] = $instance['image1'];
						$links[] = $instance["Image1Link"];
					if ($instance['image2'])
						$images[] = $instance['image2'];
						$links[] = $instance["Image2Link"];
					if ($instance['image3'])
						$images[] = $instance['image3'];
						$links[] = $instance["Image3Link"];
					if ($instance['image4'])
						$images[] = $instance['image4'];
						$links[] = $instance["Image4Link"];
					if ($instance['image5'])
						$images[] = $instance['image5'];
						$links[] = $instance["Image5Link"];
					if ($instance['image6'])
						$images[] = $instance['image6'];
						$links[] = $instance["Image6Link"];

					list($url, $width, $height) = wp_get_attachment_image_src($images[0], 'full');
					$i=0;
					
					foreach($images as $image)
					{ 
						
						list($url, $width, $height) = wp_get_attachment_image_src($image, 'full');

						if (empty($links[$i])){
							echo '<div class="item" style="vertical-align:middle;"><img width="'.$width.'" height="'.$height.'" src="'.$url.'" /></div>';
						}
						else {
							echo '<div class="item" style="vertical-align:middle;"><a href="'.$links[$i].'"><img width="'.$width.'" height="'.$height.'" src="'.$url.'" /></a></div>';
						}
						
						$i = $i+1;
					}
					
				?>
		</div>
    <script>
    jQuery(document).ready(function() {
		//Sort random function
		<?php if ($randomize){?>
		  function random(owlSelector){
			owlSelector.children().sort(function(){
				return Math.round(Math.random()) - 0.5;
			}).each(function(){
			  jQuery(this).appendTo(owlSelector);
			});
		  }
		 <?php }?>
      jQuery("#<?php echo $this->id ?>-div").owlCarousel({
        items : 2,
        itemsScaleUp:true,
		autoPlay:true,
		responsive: true,
		responsiveBaseWidth: jQuery("#<?php echo $this->id ?>")
		<?php if ($randomize){?>,
		beforeInit : function(elem){
			  //Parameter elem pointing to $("#owl-demo")
			  random(elem);
			}
		<?php }?>
      });

    });
    </script>
	</aside>
	<?php
	}
}

function owl_image_slider_scripts()
{
    wp_register_script( 'owlcarousel-js', plugins_url( '/js/owl.carousel.min.js', __FILE__ ) );
	wp_enqueue_script( 'owlcarousel-js' );
	
	wp_register_style( 'owlcarousel-css', plugins_url( '/css/owl.carousel.css', __FILE__ ) );
	wp_register_style( 'owltheme-css', plugins_url( '/css/owl.theme.css', __FILE__ ) );	
	wp_register_style( 'owltransitions-css', plugins_url( '/css/owl.transitions.css', __FILE__ ) );	
	
	wp_enqueue_style('owlcarousel-css');
	wp_enqueue_style('owltheme-css');
	wp_enqueue_style('owltransitions-css');
}
add_action( 'wp_enqueue_scripts', 'owl_image_slider_scripts' );

// register widget
add_action('widgets_init', create_function('', 'return register_widget("owl_image_slider");'));

?>